const cron = require('node-cron');
const genInvoices = require('./listeners/genInvoicesListener');

cron.schedule('* 5 * * * *', () => {
    genInvoices.genInvoice();
});