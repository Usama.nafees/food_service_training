require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
// const cors = require("cors");
const http = require('http');

const app = express();
// var corsOptions = {
//   origin: "http://localhost:8081"
// };
// app.use(cors(cors));
// require('./exceptions/globalException')
require('./scheduler')

app.use(bodyParser.json());
app.use('/', require('./routes/apis'));

const server = http.createServer(app);
const {PORT} = process.env;
server.listen(PORT, () => {
  console.log(`Restaurant open at:${PORT}`);
});

