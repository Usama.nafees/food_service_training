const models = require('../../models');

const { subscriptionRequests,Meal,User,subscriptions } = models;

const create = async (reqobj) => {
  try {
    const result = await subscriptionRequests.create(reqobj);
    return { success: true, data: result };
  } catch (error) {
    return { success: false, msg: error.toString()};
  }
};
const all = async()=>{
    try {
        const result = await subscriptionRequests.findAll({
            include:[
                {model:Meal, as:'meal', include:['mealsPerDay']},
                {model:User, as:'user'}
            ],
            where:{status:'pending'}
        });
        return { success: true, data: result };
      } catch (error) {
        return { success: false, msg: error.toString()};
      }
}
const confirmstatus = async(statusValue,requestId) =>{
    try {
        const result = await subscriptionRequests.update(
            { status:statusValue },
            {
              where: { id: requestId},
              returning: true,
              plain: true,
            }
          );
          if(statusValue === "approved" && (result))
          {
            const existingSub = await subscriptions.findOne({where:{userId:result[1].dataValues.requestedBy,mealId:result[1].dataValues.mealId}});
            if(!existingSub){
                const subobj = {
                    userId:result[1].dataValues.requestedBy,
                    mealId:result[1].dataValues.mealId,
                    status:"active"
                  }
                const newsub = await subscriptions.create(subobj);
                result[1].setDataValue('subscription', newsub);
            }else{
                result[1].setDataValue('subscription', existingSub);
            }
          }

        return { success: true, data: result };
      } catch (error) {
        return { success: false, msg: error.toString()};
      }
}

module.exports = {
    create,
    all,
    confirmstatus
};