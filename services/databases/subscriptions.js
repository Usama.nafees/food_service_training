const { Op } = require("sequelize");
const moment = require('moment');
require('moment-business-days');
const models = require('../../models');

const { Meal,User,subscriptions } = models;
const all = async()=>{
    try {
        const result = await subscriptions.findAll({
            include:[
                {model:Meal, as:'meal', include:['mealsPerDay'],
            where:{
                [Op.or]: [{endDate: {
                    [Op.gt]: moment().format('L'),
                }}, {endDate: {
                    [Op.eq]: moment().format('L'),
                }}]
            }
            },
                {model:User, as:'user'}
            ],
            where:{status:'active'}
        });
        return { success: true, data: result };
      } catch (error) {
        return { success: false, msg: error.toString()};
      }
}

module.exports = {
    all,
};