const models = require('../../models');

const { User } = models;

const allUser = async () => {
  try {
    const result = await User.findAll({
      include: 'roles',
    });
    return { success: true, data: result };
  } catch (error) {
    return { success: false, msg: error.toString() };
  }
};

module.exports = {
  allUser,
};