const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const models = require('../../models');

const {TOKEN_KEY} = process.env
const { User } = models;
const register = async (userobj) => {
  try {
    const { email } = userobj;
    const oldUser = await User.findOne({ where: { email } });

    if (oldUser) {
      return { success: false, msg: 'email already exists' };
    }
    const encryptedPassword = await bcrypt.hash(userobj.password, 10);
    userobj.password = encryptedPassword;
    const user = await User.create(userobj);

    const token = jwt.sign(
      { user_id: user.id, email },
      // process.env.TOKEN_KEY,
      TOKEN_KEY,
      {
        expiresIn: '2h',
      }
    );
    user.token = token;
    return { success: true, data: user };
  } catch (error) {
    return { success: false, msg: error.toString() };
  }
};
const login = async (userobj) => {
  try {
    const user = await User.findOne({ where: { email: userobj.email } });
    if (user && (await bcrypt.compare(userobj.password, user.password))) {
      const token = jwt.sign(
        { user_id: user.id, email: userobj.email },
        // 'abcdefg1234567',
        process.env.TOKEN_KEY,
        {
          expiresIn: '2h',
        }
      );
      user.setDataValue('token', token);
      return { success: true, data: user };
    }
    return { success: false, msg: 'Invalid credentials' };
  } catch (error) {
    return { success: false, msg: error.toString() };
  }
};
module.exports = {
  register,
  login,
};
