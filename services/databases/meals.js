const moment = require('moment');
require('moment-business-days');
const models = require('../../models');

const { Meal, MealPerDay } = models;

const addMeal = async (mealobj) => {
  try {
    const result = await Meal.create(mealobj);
    if (result) {
      const mealperday = [];
      for (let i = 0; i < 5; i++) {
        const obj = {
          mealId: result.id,
          name: '',
          date:
            i === 0
              ? moment(mealobj.startDate, 'MM-DD-YYYY').format()
              : moment(mealobj.startDate, 'MM-DD-YYYY')
                  .businessAdd(i, 'days')
                  .format(),
        };
        mealperday.push(obj);
      }
      //   console.log('following are the things here', mealperday);
      if (mealperday.length > 0) {
        const mealPerDayresult = await MealPerDay.bulkCreate(mealperday, {
          returning: true,
        });
        if (mealPerDayresult) {
          return { success: true, data: mealPerDayresult };
        }
      } else {
        return {
          success: false,
          msg: 'some thing went wrong while adding meals per day',
        };
      }
    } else {
      return { success: false, msg: 'meal could not be added' };
    }
  } catch (error) {
    return { success: false, msg: error.toString() };
  }
};

const addMealPerDay = async (mealsperday, meal_id) => {
  // try {
  if (mealsperday.length > 0) {
    const mpDay = [];
    for (let i = 0; i < mealsperday.length; i++) {
      const result = await MealPerDay.update(
        { name: mealsperday[i].name, description: mealsperday[i].description },
        {
          where: { id: mealsperday[i].id, mealId: meal_id },
          returning: true,
          plain: true,
        }
      );
      if (result) {
        mpDay.push(result);
      }
    }
    return { success: true, data: mpDay };
  }
  return { success: false, msg: 'records does not exist' };

  // } catch (error) {

  //     return {success:false, msg:error.errors.toSting()}
  // }
};

const availableMeals = async () => {
  try {
    const result = await Meal.findAll({
      include: 'mealsPerDay',
    });
    return { success: true, data: result };
  } catch (error) {
    return { success: false, data: error.toString() };
  }
};
module.exports = {
  addMeal,
  addMealPerDay,
  availableMeals,
};
