const models = require('../../models');

const { Roles } = models;

const addRole = async (roleObj) => {
  try {
    const result = await Roles.create(roleObj);
    return { success: true, data: result };
  } catch (error) {
    return { success: false, data: error.toString() };
  }
};
module.exports = {
  addRole,
};
