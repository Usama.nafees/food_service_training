const nodemailer = require('nodemailer');

const {EMAIL_P} = process.env
// here i can also get all these attributes from env file
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'usama.nafees@invozone.com',
    pass: EMAIL_P

  }
});

const mailOptions = {
  from: 'usama.nafees@invozone.com',
  to: 'usama.nafees.u@gmail.com',
  subject: 'Auto Generated subscription Invoice',
  html: '<div>data is missing</div>'
};

module.exports = {
    transporter,
    mailOptions
}