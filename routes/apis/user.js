const router = require('express').Router();
const usercontroller = require('../../controllers/UserController');
const authorize = require('../../middlewares/authorize');

router.get('/all', authorize, usercontroller.allUser);

module.exports = router;
