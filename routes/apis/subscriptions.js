const router = require('express').Router();
const requestController = require('../../controllers/SubscriptionController');
const authorize = require('../../middlewares/authorize');

router.get('/all',authorize,requestController.all);

module.exports = router;