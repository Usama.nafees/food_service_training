const router = require('express').Router();
const mealcontroller = require('../../controllers/MealsController');
const authorize = require('../../middlewares/authorize');

router.post('/addMeal', authorize, mealcontroller.addMeal);
router.put('/addMealperday/:id', authorize, mealcontroller.addMealPerDay);
router.get('/availableMeals', mealcontroller.availableMeals);

// router.get('/getUsers',auth, usercontroller.getUsers)

module.exports = router;
