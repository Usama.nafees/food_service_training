const router = require('express').Router();
const requestController = require('../../controllers/RequestController');
const authorize = require('../../middlewares/authorize');

router.post('/create', requestController.create);
router.get('/all',authorize, requestController.all);
router.post('/confirmstatus',authorize, requestController.confirmstatus);

module.exports = router;