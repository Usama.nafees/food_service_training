const router = require('express').Router();
const auth = require('../../middlewares/jwtauth');
// Route for users

router.use('/user', auth, require('./user'));
// router.use('/role', require('./roles'))
router.use('/auth', require('./auth'));
router.use('/meal', auth, require('./meals'));
router.use('/request', auth, require('./subscriptionRequests'));
router.use('/subscription', auth, require('./subscriptions'));

module.exports = router;