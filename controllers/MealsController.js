const moment = require('moment');
require('moment-business-days');
const mealServices = require('../services/databases/meals');

const addMeal = async (req, res) => {
  const { name, type, startDate } = req.body;
  if (type === '' || !type) {
    res.status(400).send({ result: false, msg: 'Name is requried' });
  }
  const endDate = moment(startDate, 'MM-DD-YYYY')
    .businessAdd(4, 'days')
    .format('L');
  const mealobj = {
    name,
    type,
    status: 0,
    startDate,
    endDate,
    createdBy: req.user.user_id,
  };
  console.log('this is user id comming in the request', mealobj, req.user.id);
  const result = await mealServices.addMeal(mealobj);
  if (result.success) res.status(200).send(result);
  else res.status(502).send(result);
};
const addMealPerDay = async (req, res) => {
  const { mealsperday } = req.body;
  const result = await mealServices.addMealPerDay(mealsperday, req.params.id);
  if (result.success) res.status(200).send(result);
  else res.status(502).send(result);
};
const availableMeals = async (req, res) => {
  const result = await mealServices.availableMeals();
  if (result.success) res.status(200).send(result);
  else res.status(502).send(result);
};

module.exports = {
  addMeal,
  addMealPerDay,
  availableMeals,
};
