const AuthService = require('../services/databases/auth');

const register = async (req, res) => {
  const { firstName, lastName, email, password } = req.body;
  if (!(firstName && lastName && email && password)) {
    res.status(400).send('All inputs are required');
  }
  const userobj = {
    firstName,
    lastName,
    email,
    password,
  };
  const result = await AuthService.register(userobj);
  if (result.success) res.status(201).send(result);
  else res.status(400).send(result);
};
const login = async (req, res) => {
  const { email, password } = req.body;
  if (!(email && password)) {
    res.status(400).send('email and password are required');
  }
  const userobj = {
    email,
    password,
  };
  const result = await AuthService.login(userobj);
  if (result.success) res.status(200).send(result);
  else res.status(400).send(result);
};
module.exports = {
  register,
  login,
};
