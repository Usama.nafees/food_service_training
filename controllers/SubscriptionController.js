const subscriptions = require('../services/databases/subscriptions');

const all = async(req,res)=>{
    const result = await subscriptions.all();
    if (result.success) res.status(200).send(result);
    else res.status(502).send(result);
}

module.exports = {
    all,
}