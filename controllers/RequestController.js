const subRequestServices = require('../services/databases/subscriptionRequest');

const create = async(req,res)=>{
    const {mealId,proof_link} = req.body;
    const reqobj ={
        mealId,
        proof_link,
        requestedBy : req.user.user_id,
        status:'pending'
    }
    const result = await subRequestServices.create(reqobj);
    if (result.success) res.status(200).send(result);
    else res.status(502).send(result);
}
const all = async(req,res)=>{
    const result = await subRequestServices.all();
    if (result.success) res.status(200).send(result);
    else res.status(502).send(result);
}
const confirmstatus = async(req,res) =>{
    const {status,requestId} = req.body;
    const result = await subRequestServices.confirmstatus(status,requestId);
    if (result.success) res.status(200).send(result);
    else res.status(502).send(result);
}
module.exports = {
    create,
    all,
    confirmstatus
}