const userServices = require('../services/databases/users');

const allUser = async (req, res) => {
  const result = await userServices.allUser();
  if (result.success) res.status(200).send(result);
  else res.status(502).send(result);
};

module.exports = {
  allUser,
};
