const models = require('../models');

const { User } = models;

const authoriz = async (req, res, next) => {
  const user = await User.findOne({
    include: ['roles'],
    where: { id: req.user.user_id },
  });
  const found = user.roles.some((el) => el.name === 'admin');
  if (!found) {
    return res.status(403).send('unauthorized access');
  }
  return next();
};
module.exports = authoriz;
