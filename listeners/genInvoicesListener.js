const subscriptions = require('../services/databases/subscriptions');
const invoicemailer = require('../mailer/invoiceTemplate');

const EventEmitter = require("../events/genInvoicesEvent")

const eventemmiter = new EventEmitter();

eventemmiter.on('genInvoice', async()=>{
    const result = await subscriptions.all();
    if (result.success) {
        invoicemailer.mailOptions.html = result.data.toString() // here i can format this data however i want
    }else invoicemailer.mailOptions.html  = '<div>data is missing</div>'

    invoicemailer.transporter.sendMail(invoicemailer.mailOptions, async(error, info) =>{
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' , info.response);
        }
      });
});
module.exports = eventemmiter;
// eventemmiter.genInvoice();