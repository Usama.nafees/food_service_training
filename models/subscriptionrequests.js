'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class subscriptionRequests extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      subscriptionRequests.belongsTo(models.User, {
        as: 'user',
        foreignKey: 'requestedBy',
      });
      subscriptionRequests.belongsTo(models.Meal, {
        as: 'meal',
        foreignKey: 'mealId',
      });
    }
  }
  subscriptionRequests.init({
    requestedBy: DataTypes.INTEGER,
    mealId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    proof_link: DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'subscriptionRequests',
  });
  return subscriptionRequests;
};