const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.belongsToMany(models.Role, {
        as: 'roles',
        through: models.UserRole,
        foreignKey: 'user_id',
      });
      // define association here
    }
  }
  User.init(
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        validate: {
          contains: 'test', // force specific substrings
          isEmail: true,
        },
      },
      password: DataTypes.STRING,
    },
    
    {
      hooks: {
        afterCreate : async (created_user, options) =>{
          const roles = await sequelize.models.Role.findOne({
            where: {name:'customer'}
          });
            const userrole = {
              role_id:roles.id,
              user_id:created_user.id,
            }
            await sequelize.models.UserRole.create(userrole)
        }
      },
      sequelize,
      paranoid: true, // for soft delete
      modelName: 'User',
    }
  );
  return User;
};
