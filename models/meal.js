const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Meal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Meal.hasMany(models.MealPerDay, {
        as: 'mealsPerDay',
        foreignKey: 'mealId',
      });
    }
  }
  Meal.init(
    {
      name: DataTypes.STRING,
      type: DataTypes.STRING,
      status: DataTypes.BOOLEAN,
      startDate: DataTypes.STRING,
      endDate: DataTypes.STRING,
      deletedAt: DataTypes.DATE,
      createdBy: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'Meal',
    }
  );
  return Meal;
};
