'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class subscriptions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      subscriptions.belongsTo(models.User, {
        as: 'user',
        foreignKey: 'userId',
      });
      subscriptions.belongsTo(models.Meal, {
        as: 'meal',
        foreignKey: 'mealId',
      });
    }
  }
  subscriptions.init({
    userId: DataTypes.INTEGER,
    mealId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'subscriptions',
  });
  return subscriptions;
};