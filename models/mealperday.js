const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class MealPerDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      MealPerDay.belongsTo(models.Meal, {
        as: 'meals',
        foreignKey: 'mealId',
      });
    }
  }
  MealPerDay.init(
    {
      mealId: DataTypes.INTEGER,
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      date: DataTypes.STRING,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'MealPerDay',
    }
  );
  return MealPerDay;
};
